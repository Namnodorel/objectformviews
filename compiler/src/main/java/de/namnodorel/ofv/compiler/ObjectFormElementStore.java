package de.namnodorel.ofv.compiler;

import com.squareup.javapoet.TypeName;

import javax.lang.model.element.TypeElement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class ObjectFormElementStore {

    private ObjectFormElementStore(){}

    private static Map<String, TypeElement> objectFormPartClassMap = new HashMap<>();

    public static void setObjectFormPart(String forTypeName, TypeElement objectFormPart){
        objectFormPartClassMap.put(forTypeName, objectFormPart);
    }

    public static TypeElement getObjectFormPart(TypeName ofType){
        for(String typeName : objectFormPartClassMap.keySet()){
            if(typeName.equals(ofType.toString())){
                return objectFormPartClassMap.get(typeName);
            }
        }
        throw new IllegalArgumentException("There is no ObjectFormElement for " + ofType.toString());
    }

    public static boolean hasObjectFormPart(TypeName ofType){
        for(String typeName : objectFormPartClassMap.keySet()){
            if(typeName.equals(ofType.toString())){
                return true;
            }
        }
        return false;
    }

    public static boolean hasObjectFormParts(){
        return objectFormPartClassMap.size() > 0;
    }

    public static Collection<TypeElement> getStoredTypes(){
        return objectFormPartClassMap.values();
    }
}
