package de.namnodorel.ofv.compiler;

import com.google.auto.common.BasicAnnotationProcessor;
import com.google.auto.common.MoreTypes;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.*;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import de.namnodorel.ofv.ResultProvider;
import de.namnodorel.ofv.ValueChangeCallback;
import de.namnodorel.ofv.annotations.GenerateObjectForm;
import de.namnodorel.ofv.annotations.NoFormField;
import de.namnodorel.ofv.annotations.ObjectFormElement;

import static javax.lang.model.element.Modifier.ABSTRACT;
import static javax.lang.model.element.Modifier.FINAL;
import static javax.lang.model.element.Modifier.PRIVATE;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;
import static javax.tools.Diagnostic.Kind.ERROR;
import static javax.tools.Diagnostic.Kind.WARNING;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({
        "de.namnodorel.ofv.annotations.ObjectFormElement",
        "de.namnodorel.ofv.annotations.GenerateObjectForm"
})
public class OFVProcessor extends BasicAnnotationProcessor {

    private static final String PACKAGE = "de.namnodorel.ofv";
    private static final ClassName ABSTRACT_GENERATOR_CLASS_NAME = ClassName.get(PACKAGE, "FormViewGenerator");

    private static final ClassName VIEW = ClassName.get("android.view", "View");
    private static final ClassName CONTEXT = ClassName.get("android.content", "Context");
    private static final ClassName LINEAR_LAYOUT = ClassName.get("android.widget", "LinearLayout");

    private Filer filer;
    private Messager messager;
    private Elements elementUtils;
    private Types typeUtils;

    @Override
    protected Iterable<? extends ProcessingStep> initSteps() {

        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
        elementUtils = processingEnv.getElementUtils();
        typeUtils = processingEnv.getTypeUtils();

        Set<ProcessingStep> steps= new HashSet<>();

        steps.add(new ProcessingStep() {

            @Override
            public Set<? extends Class<? extends Annotation>> annotations() {
                return Sets.newHashSet(ObjectFormElement.class);
            }

            @Override
            public Set<? extends Element> process(SetMultimap<Class<? extends Annotation>, Element> elementsByAnnotation) {
                collectObjectFormElements(elementsByAnnotation.get(ObjectFormElement.class));

                generateParameterAnnotations();

                return new HashSet<>();
            }

        });


        steps.add(new ProcessingStep() {

            @Override
            public Set<? extends Class<? extends Annotation>> annotations() {
                return Sets.newHashSet(GenerateObjectForm.class);
            }

            @Override
            public Set<? extends Element> process(SetMultimap<Class<? extends Annotation>, Element> elementsByAnnotation) {

                Set<Element> elements = elementsByAnnotation.get(GenerateObjectForm.class);

                if (ObjectFormElementStore.hasObjectFormParts()) {

                    try {

                        Map<ClassName, ClassName> resultGeneratorMap = new HashMap<>();

                        for (Element el : elements) {

                            TypeElement annotatedClass = (TypeElement) el;

                            ClassName resultClassName = ClassName.get(annotatedClass);

                            ClassName generatedClassName = generateFormViewGeneratorClass(annotatedClass);

                            resultGeneratorMap.put(resultClassName, generatedClassName);
                        }

                        generateAbstractGeneratorClass(resultGeneratorMap);

                        return new HashSet<>();

                    } catch (IllegalStateException ex) {
                        //The given elements (and their enclosed elements) aren't well-defined enough yet.
                        //Defer all elements until later.
                        return elements;
                    }catch (Exception ex){
                        messager.printMessage(ERROR, "UNEXPECTED EXCEPTION: " + ex.getMessage());
                        ex.printStackTrace();
                        return elements;
                    }

                } else {
                    return elements;
                }

            }
        });

        return steps;
    }

    private void generateParameterAnnotations() {
        for(TypeElement formElementType : ObjectFormElementStore.getStoredTypes()){
            List<VariableElement> fields = getFields(formElementType);

            Set<MethodSpec> parameters = new HashSet<>();

            for(VariableElement field : fields){
                TypeName fieldTypeClassName = TypeName.get(field.asType());

                MethodSpec parameterMethod = MethodSpec.methodBuilder(field.getSimpleName().toString())
                        .returns(fieldTypeClassName)
                        .addModifiers(PUBLIC, ABSTRACT)
                        .build();

                parameters.add(parameterMethod);
            }

            if(parameters.size() > 0){
                String annotationName = "FormParam" + formElementType.getSimpleName();

                AnnotationSpec retention = AnnotationSpec.builder(Retention.class)
                        .addMember("value", "$T.RUNTIME", RetentionPolicy.class)
                        .build();

                AnnotationSpec target = AnnotationSpec.builder(Target.class)
                        .addMember("value", "$T.FIELD", ElementType.class)
                        .build();

                TypeSpec formElementParamAnnotation = TypeSpec.annotationBuilder(annotationName)
                        .addAnnotation(retention)
                        .addAnnotation(target)
                        .addModifiers(PUBLIC)
                        .addMethods(parameters)
                        .build();


                String packageName = elementUtils.getPackageOf(formElementType).getQualifiedName().toString();

                try {
                    JavaFile.builder(packageName, formElementParamAnnotation)
                            .build()
                            .writeTo(filer);
                } catch (IOException e) {
                    messager.printMessage(WARNING, e.getMessage());
                }
            }
        }
    }

    private ClassName generateFormViewGeneratorClass(TypeElement forTypeElement) {

        ClassName resultClassName = ClassName.get(forTypeElement);

        List<VariableElement> fields = getFields(forTypeElement);

        String packageName = resultClassName.packageName();
        ClassName interfaceName = ClassName.get(PACKAGE, "ValueChangeCallback");

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addModifiers(PUBLIC)
                .addParameter(resultClassName, "prefill")
                .beginControlFlow("if(prefill == null)")
                .addStatement("result = new $T()", resultClassName)
                .nextControlFlow("else")
                .addStatement("result = prefill")
                .endControlFlow()
                .build();

        MethodSpec getResultMethod = MethodSpec.methodBuilder("getResult")
                .addModifiers(PUBLIC)
                .returns(resultClassName)
                .addStatement("return result")
                .build();

        MethodSpec countMethod = generateCountMethod(fields);
        MethodSpec formMethod = generateFormMethod(resultClassName, interfaceName, fields);

        String generatedName = resultClassName.simpleName() + "FormViewGen";
        TypeSpec generatedClass = TypeSpec.classBuilder(generatedName)
                .addModifiers(PUBLIC, FINAL)
                .superclass(ParameterizedTypeName.get(
                        ABSTRACT_GENERATOR_CLASS_NAME,
                        resultClassName
                ))
                .addField(resultClassName, "result", PRIVATE)
                .addMethod(constructor)
                .addMethod(countMethod)
                .addMethod(formMethod)
                .addMethod(getResultMethod)
                .build();


        try {
            JavaFile.builder(packageName, generatedClass)
                    .build()
                    .writeTo(filer);
        } catch (IOException e) {
            messager.printMessage(WARNING, e.getMessage());
        }

        return ClassName.get(packageName, generatedName);
    }

    private void generateAbstractGeneratorClass(Map<ClassName, ClassName> resultGeneratorMap) {
        ClassName resultProviderInterfaceName = ClassName.get(ResultProvider.class);

        TypeVariableName resultTypeVariableName = TypeVariableName.get("T");

        MethodSpec generateFormViewAbstractMethod = MethodSpec.methodBuilder("generateFormView")
                .addModifiers(PUBLIC, ABSTRACT)
                .returns(VIEW)
                .addParameter(CONTEXT, "context")
                .build();

        MethodSpec getResultAbstractMethod = MethodSpec.methodBuilder("getResult")
                .addModifiers(PUBLIC, ABSTRACT)
                .returns(resultTypeVariableName)
                .build();


        MethodSpec countAbstractMethod = MethodSpec.methodBuilder("countAttributes")
                .addModifiers(PUBLIC, ABSTRACT)
                .returns(TypeName.INT)
                .build();


        MethodSpec findMethod = generateFindFormGeneratorMethod(resultGeneratorMap);

        TypeSpec abstractClass = TypeSpec.classBuilder(ABSTRACT_GENERATOR_CLASS_NAME.simpleName())
                .addModifiers(PUBLIC, ABSTRACT)
                .addTypeVariable(resultTypeVariableName)
                .addSuperinterface(ParameterizedTypeName.get(
                        resultProviderInterfaceName,
                        resultTypeVariableName
                ))
                .addMethod(generateFormViewAbstractMethod)
                .addMethod(getResultAbstractMethod)
                .addMethod(countAbstractMethod)
                .addMethod(findMethod)
                .build();

        try {
            JavaFile.builder(PACKAGE, abstractClass)
                    .build()
                    .writeTo(filer);
        } catch (IOException e) {
            messager.printMessage(WARNING, e.getMessage());
        }
    }

    private MethodSpec generateFindFormGeneratorMethod(Map<ClassName, ClassName> resultGeneratorMap) {
        TypeVariableName inputTypeVariable = TypeVariableName.get("T");

        MethodSpec.Builder findMethodBuilder = MethodSpec.methodBuilder("find")
                .addModifiers(PUBLIC, STATIC)
                .addTypeVariable(inputTypeVariable)
                .returns(ParameterizedTypeName.get(
                        ABSTRACT_GENERATOR_CLASS_NAME,
                        inputTypeVariable
                ))
                .addParameter(inputTypeVariable, "input");

        for(ClassName subjectClassName : resultGeneratorMap.keySet()){
            ClassName generatorClassName = resultGeneratorMap.get(subjectClassName);

            findMethodBuilder
                    .beginControlFlow("if(input instanceof $T)", subjectClassName)
                    .addStatement("return ($T<$T>) new $T(($T)input)", ABSTRACT_GENERATOR_CLASS_NAME, inputTypeVariable, generatorClassName, subjectClassName)
                    .endControlFlow();
        }

        ClassName illegalArgumentExceptionClassName = ClassName.get(IllegalArgumentException.class);
        findMethodBuilder
                .addStatement("throw new $T($S + input.getClass().getName())", illegalArgumentExceptionClassName, "There is no form generator for type ");

        return findMethodBuilder.build();
    }

    private void collectObjectFormElements(Set<Element> fromElements) {
        for(Element el : fromElements){

            TypeElement annotatedClass = (TypeElement)el;

            String target = annotatedClass.getAnnotation(ObjectFormElement.class).value();
            ObjectFormElementStore.setObjectFormPart(target, annotatedClass);

        }
    }

    private MethodSpec generateCountMethod(List<VariableElement> fields){
        final String METHOD_NAME = "countAttributes";

        int count = 0;
        for(VariableElement field: fields){
            if(field.getAnnotation(NoFormField.class) == null){
                count++;
            }
        }

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(METHOD_NAME)
                .addModifiers(PUBLIC)
                .returns(TypeName.INT)
                .addStatement("return " + count);

        return methodBuilder.build();
    }

    private MethodSpec generateFormMethod(ClassName clsName, TypeName attrCallbackInterface, List<VariableElement> fields){

        final String METHOD_NAME = "generateFormView";

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(METHOD_NAME)
                .addModifiers(PUBLIC)
                .addParameter(CONTEXT, "context")
                .returns(VIEW)
                .addStatement("$T layout = new $T(context)", LINEAR_LAYOUT, LINEAR_LAYOUT)
                .addStatement("layout.setOrientation($T.VERTICAL)", LINEAR_LAYOUT);

        for(VariableElement field : fields){

            if((field.getAnnotation(NoFormField.class) != null)){
                continue;
            }

            String attrName = field.getSimpleName().toString();
            String attrNameCapitalized = attrName.substring(0, 1).toUpperCase() + attrName.substring(1);

            TypeName attrTypeName = TypeName.get(field.asType());

            methodBuilder.addStatement("$T $LChangeCallback = (val) -> result.$L(($T)val)", attrCallbackInterface, attrName, "set" + attrNameCapitalized, attrTypeName);

            TypeElement formElement = ObjectFormElementStore.getObjectFormPart(attrTypeName);

            methodBuilder.addStatement("$T $LFormElement = new $T()", formElement, attrName, formElement);

            for (AnnotationMirror annotation : field.getAnnotationMirrors()) {

                if(annotation.getAnnotationType().getKind().equals(TypeKind.ERROR) || annotation.getAnnotationType().asElement().getSimpleName().toString().contains("NonExistentClass")){
                    throw new IllegalStateException("Not well formed yet! (" + annotation.getAnnotationType().toString() + ")");
                }

                Map<? extends ExecutableElement, ? extends AnnotationValue> annotationValueMap = annotation.getElementValues();

                for(ExecutableElement valueMethod : annotationValueMap.keySet()){

                    AnnotationValue annotationValue = annotationValueMap.get(valueMethod);

                    String methodName = valueMethod.getSimpleName().toString();
                    String valueNameCapitalized = methodName.substring(0, 1).toUpperCase() + methodName.substring(1);

                    String javaPoetKey = "$L";
                    Object valueInJava = "";
                    String postString = "";
                    if(annotationValue.getValue() instanceof String){
                        javaPoetKey = "$S";
                        valueInJava = annotationValue.getValue();

                    }else if(annotationValue.getValue() instanceof TypeMirror){
                        javaPoetKey = "$T";
                        valueInJava = ClassName.get((TypeMirror) annotationValue.getValue());
                        postString = ".class";

                    }else if(annotationValue.getValue() instanceof Integer || annotationValue.getValue() instanceof Boolean){
                        valueInJava = String.valueOf(annotationValue.getValue());

                    }else{
                        messager.printMessage(WARNING, "Can't assign parameter of type " + annotationValue.getValue().getClass().toString());
                    }

                    methodBuilder.addStatement("$LFormElement.set$L(" + javaPoetKey + postString + ")", attrName, valueNameCapitalized, valueInJava);

                }

            }


            ExecutableElement formPartElementViewMethod = getAttachViewMethod(formElement);

            methodBuilder.addStatement("$LFormElement.$L(result.$L(), $S, $LChangeCallback, layout)", attrName, formPartElementViewMethod.getSimpleName(), "get" + attrNameCapitalized, clsName.simpleName().toLowerCase() + "_" + attrName.toLowerCase(), attrName);
        }

        methodBuilder.addStatement("return layout");

        return methodBuilder.build();
    }

    private ExecutableElement getAttachViewMethod(TypeElement ofClass) {
        ExecutableElement formPartElementViewMethod = null;
        for(Element el : ofClass.getEnclosedElements()){
            if(el.getKind().equals(ElementKind.METHOD)){
                ExecutableElement method = (ExecutableElement) el;

                if(method.getParameters().size() == 4){

                    //Is this stupid? Yes. Does isSameType() otherwise return false, even though the type is exactly the same? Yes!
                    TypeMirror param0 = method.getParameters().get(0).asType();
                    TypeMirror param1 = method.getParameters().get(1).asType();
                    TypeMirror param2 = method.getParameters().get(2).asType();
                    TypeMirror param3 = method.getParameters().get(3).asType();

                    boolean takesObjectFirst = MoreTypes.isTypeOf(Object.class, param0);
                    boolean takesStringSecond = MoreTypes.isTypeOf(String.class, param1);
                    boolean takesCallbackThird = MoreTypes.isTypeOf(ValueChangeCallback.class, param2);
                    boolean takesViewGroupFourth = param3.toString().equals("android.view.ViewGroup");

                    if(takesObjectFirst && takesStringSecond && takesCallbackThird && takesViewGroupFourth){

                        if(formPartElementViewMethod == null){
                            formPartElementViewMethod = method;
                        }else{
                            messager.printMessage(ERROR, ofClass.getSimpleName().toString() + " has more than one method(Object, String, ValueChangeCallback, ViewGroup)");
                        }
                    }
                }
            }
        }

        if(formPartElementViewMethod == null){
            messager.printMessage(ERROR, ofClass.getSimpleName().toString() + " has no method(Object, String, ValueChangeCallback, ViewGroup)");
        }
        return formPartElementViewMethod;
    }

    private List<VariableElement> getFields(TypeElement withinClass){
        List<VariableElement> fields = new LinkedList<>();

        if(withinClass.getSuperclass().getKind() != TypeKind.NONE){
            fields.addAll(getFields((TypeElement) typeUtils.asElement(withinClass.getSuperclass())));
        }

        for(Element enclosedElement : withinClass.getEnclosedElements()){

            if(enclosedElement.getKind().equals(ElementKind.FIELD)
                    && !enclosedElement.getModifiers().contains(STATIC)){

                fields.add((VariableElement) enclosedElement);
            }

        }

        return fields;
    }
}
