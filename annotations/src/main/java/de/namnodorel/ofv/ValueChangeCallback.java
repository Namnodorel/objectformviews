package de.namnodorel.ofv;

public interface ValueChangeCallback {
    void updateVariable(Object val);
}
