package de.namnodorel.ofv;

public interface ResultProvider<T> {

    T getResult();

}
